#ifndef ClawStopCmd_H
#define ClawStopCmd_H

#include "../CommandBase.h"
#include "WPILib.h"


const float CLAW_STOP_CMD_TIMEOUT  = 0.0;  // No timeout

class ClawStopCmd: public CommandBase
{
public:
	ClawStopCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	float CommandTimeout;
};

#endif

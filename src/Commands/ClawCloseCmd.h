#ifndef ClawCloseCmd_H
#define ClawCloseCmd_H

#include "../CommandBase.h"
#include "WPILib.h"

const float CLAW_CLOSE_CMD_TIMEOUT  = 2.0;


class ClawCloseCmd: public CommandBase
{
public:
	ClawCloseCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	float CommandTimeout;
};

#endif

#include "ClawOpenCmd.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"


ClawOpenCmd::ClawOpenCmd() : CommandBase("ClawOpenCmd"), CommandTimeout(CLAW_OPEN_CMD_TIMEOUT)
{
	// Use Requires() here to declare subsystem dependencies
	Requires(clawSystem);
	SetTimeout(CommandTimeout);
}

// Called just before this Command runs the first time
void ClawOpenCmd::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void ClawOpenCmd::Execute()
{
	clawSystem->Open();
}

// Make this return true when this Command no longer needs to run execute()
bool ClawOpenCmd::IsFinished()
{
	// default to say the move is finished to stop movement
	bool return_value = true;

	// If the move is not timed out continue the move
	if ( !(IsTimedOut()) )
	{
		return_value = false;
	}

	return (return_value);
}

// Called once after isFinished returns true
void ClawOpenCmd::End()
{
	clawSystem->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void ClawOpenCmd::Interrupted()
{
	clawSystem->Stop();
}

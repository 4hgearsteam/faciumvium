/*
 * ReadPiInputCmd.cpp
 *
 *  Created on: Feb 28, 2017
 *      Author: User
 */

#include "ReadPiInputCmd.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"
#include "../CommandBase.h"
#include "DriveCmd.h"

ReadPiInputCmd::ReadPiInputCmd() : CommandBase("Pi Input Reading"), CommandTimeout(0.0) {
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(Robot::chassis.get());
	Requires(PiInput);
	SetTimeout(CommandTimeout);

}

// Called just before this Command runs the first time
void ReadPiInputCmd::Execute() {
	int state = 0;
	uint32_t input = PiInput->ReadInput();
	SmartDashboard::PutNumber("Pi Input Value", input);
	//SmartDashboard::PutNumber("Current State", state);

	switch(input)
    {
        case 0:
        	//driveSystem->Drive(0, 0, 0);
        	if (state == 3)
        	{
            	driveSystem->Drive(.5, 0, 0);
        	}
        	else if (state == 2)
        	{
            	driveSystem->Drive(0,.3, 0.0);
        	}
        	else if (state == 1)
        	{
            	driveSystem->Drive(0, -.3, 0.0);
        	}
        	else
        	{
        		driveSystem->Drive(-.3, 0.0, 0.0);
        	}
        	break;

        case 1:
        	driveSystem->Drive(0, .3, 0.0);
        	std::cout<<"1";
        	state = 1;
        	break;
        case 2:
        	driveSystem->Drive(0, -.3, 0.0);
        	std::cout<<"2";
        	state = 2;
        	break;
        case 3:
        	driveSystem->Drive(.5, 0, 0);
        	std::cout<<"3";
        	state = 3;
        	break;
        default:
        	//driveSystem->Drive(0, 0, 0);
        	if (state == 3)
        	{
            	driveSystem->Drive(.5, 0, 0);
        	}
        	else if (state == 2)
        	{
            	driveSystem->Drive(0,.3, 0.0);
        	}
        	else if (state == 1)
        	{
            	driveSystem->Drive(0, -.3, 0.0);
        	}
        	break;

    }
}

bool ReadPiInputCmd::IsFinished()
{
    return true;
}
void ReadPiInputCmd::Start()
{
	Execute();
}

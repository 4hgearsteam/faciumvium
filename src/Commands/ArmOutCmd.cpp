#include "ArmOutCmd.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"


ArmOutCmd::ArmOutCmd() : CommandBase("ArmOutCmd"), CommandTimeout(ARM_OUT_CMD_TIMEOUT)
{
	// Use Requires() here to declare subsystem dependencies
	Requires(armSystem);
	SetTimeout(CommandTimeout);
}

// Called just before this Command runs the first time
void ArmOutCmd::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void ArmOutCmd::Execute()
{
	armSystem->Out();
}

// Make this return true when this Command no longer needs to run execute()
bool ArmOutCmd::IsFinished()
{
	// default to say the move is finished to stop movement
	bool return_value = true;

	// If the move is not timed out continue the move
	if ( !(IsTimedOut()) )
	{
		return_value = false;
	}

	return (return_value);
}

// Called once after isFinished returns true
void ArmOutCmd::End()
{
	armSystem->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void ArmOutCmd::Interrupted()
{
	armSystem->Stop();
}

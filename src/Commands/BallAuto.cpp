/*
 * BallAuto.cpp
 *
 *  Created on: Mar 16, 2017
 *      Author: User
 */

#include "BallAuto.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"
#include "../CommandBase.h"
#include "Subsystems/BallEgressSystem.h"
#include "Subsystems/UltrasonicSensor.h"

BallAuto::BallAuto() : CommandBase("Pi Input Reading"), CommandTimeout(0.0){
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(Robot::chassis.get());
	Requires(ballEgressSystem);
	SetTimeout(CommandTimeout);

}

// Called just before this Command runs the first time
void BallAuto::Execute() {
	ballEgressSystem->Toggle();
}

bool BallAuto::IsFinished()
{
    return true;
}
void BallAuto::Start()
{
	Execute();
}





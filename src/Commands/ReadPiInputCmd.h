/*
 * ReadPiInputCmd.h

 *
 *  Created on: Feb 28, 2017
 *      Author: User
 */

#ifndef SRC_COMMANDS_READPIINPUTCMD_H_
#define SRC_COMMANDS_READPIINPUTCMD_H_

#include <Commands/CommandGroup.h>
#include "../CommandBase.h"
#include "WPILib.h"

class ReadPiInputCmd : public CommandBase {
public:
	ReadPiInputCmd();
	void Execute();
	bool IsFinished();
	void Start();

private:
	float CommandTimeout;

};



#endif /* SRC_COMMANDS_READPIINPUTCMD_H_ */

#include "WinchPullCmd.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"

WinchPullCmd::WinchPullCmd() : CommandBase("WinchCmd"), CommandTimeout(WINCH_PULL_CMD_TIMEOUT) {
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(Robot::chassis.get());
	// Use Requires() here to declare subsystem dependencies
	Requires(winchSystem);
	// SetTimeout(CommandTimeout);
}

// Called just before this Command runs the first time
void WinchPullCmd::Initialize() {
}

// Called repeatedly when this Command is scheduled to run
void WinchPullCmd::Execute() {
	Joystick& joystick = CommandBase::oi->get_joystick();
//	Joystick& joystick = CommandBase::oi->get_joystick();
//	static bool new_b=0;
//
//		new_b = joystick.GetRawButton(TRIGGER_BUTTON_RIGHT);
//	}
	winchSystem->Move(joystick.GetRawAxis(TRIGGER_BUTTON_RIGHT));

}

// Make this return true when this Command no longer needs to run execute()
bool WinchPullCmd::IsFinished() {
	return false;
}

// Called once after isFinished returns true
void WinchPullCmd::End() {
    winchSystem->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void WinchPullCmd::Interrupted() {
    winchSystem->Stop();

}

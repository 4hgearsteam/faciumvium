#include "ArmStopCmd.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"


ArmStopCmd::ArmStopCmd() : CommandBase("ArmStopCmd"), CommandTimeout(ARM_STOP_CMD_TIMEOUT)
{
	// Use Requires() here to declare subsystem dependencies
	Requires(armSystem);
	SetTimeout(CommandTimeout);
}

// Called just before this Command runs the first time
void ArmStopCmd::Initialize()
{
}

// Called repeatedly when this Command is scheduled to run
void ArmStopCmd::Execute()
{
	armSystem->Stop();
}

// Make this return true when this Command no longer needs to run execute()
bool ArmStopCmd::IsFinished()
{
	return false;
}

// Called once after isFinished returns true
void ArmStopCmd::End()
{
	armSystem->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void ArmStopCmd::Interrupted()
{
	armSystem->Stop();
}

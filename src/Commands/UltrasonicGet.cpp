#include "UltrasonicGet.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"


UltrasonicGet::UltrasonicGet(): CommandBase("UltrasonicGet"), CommandTimeout(ULTRASONIC_TIMEOUT)
{
	// Use Requires() here to declare subsystem dependencies
	Requires(ultraSonicSensor);
	SetTimeout(CommandTimeout);
}

// Called just before this Command runs the first time
void UltrasonicGet::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void UltrasonicGet::Execute()
{
	ultraSonicSensor->SendDistance();
}

// Make this return true when this Command no longer needs to run execute()
bool UltrasonicGet::IsFinished()
{
	// default to say the move is finished to stop movement
	bool return_value = true;

	// If the move is not timed out continue the move
	if ( !(IsTimedOut()) )
	{
		return_value = false;
	}

	return (return_value);
}

// Called once after isFinished returns true
void UltrasonicGet::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void UltrasonicGet::Interrupted()
{

}

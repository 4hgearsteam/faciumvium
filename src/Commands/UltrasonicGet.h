#ifndef UltrasonicGet_H
#define UltrasonicGet_H

#include "../CommandBase.h"
#include "WPILib.h"


const float ULTRASONIC_TIMEOUT  = 0.0;  // No timeout

class UltrasonicGet: public CommandBase
{
public:
	UltrasonicGet();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	float CommandTimeout;
};

#endif

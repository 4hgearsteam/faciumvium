#ifndef WinchArmCmd_H
#define WinchArmCmd_H

#include "../CommandBase.h"

class WinchArmCmd : public CommandBase {
public:
	WinchArmCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif  // WinchArmCmd_H

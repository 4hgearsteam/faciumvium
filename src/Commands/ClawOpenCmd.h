#ifndef ClawOpenCmd_H
#define ClawOpenCmd_H

#include "../CommandBase.h"
#include "WPILib.h"

const float CLAW_OPEN_CMD_TIMEOUT  = 2.0;

class ClawOpenCmd: public CommandBase
{
public:
	ClawOpenCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	float CommandTimeout;
};

#endif

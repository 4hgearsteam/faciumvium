/*
 * BallAuto.h
 *
 *  Created on: Mar 16, 2017
 *      Author: User
 */

#ifndef SRC_COMMANDS_BALLAUTO_H_
#define SRC_COMMANDS_BALLAUTO_H_
#include <Commands/CommandGroup.h>
#include "../CommandBase.h"
#include "WPILib.h"

class BallAuto : public CommandBase {
public:
	BallAuto();
	void Execute();
	bool IsFinished();
	void Start();

private:
	float CommandTimeout;


};



#endif

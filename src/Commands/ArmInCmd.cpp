#include "ArmInCmd.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"


ArmInCmd::ArmInCmd() : CommandBase("ArmInCmd"), CommandTimeout(ARM_IN_CMD_TIMEOUT)
{
	// Use Requires() here to declare subsystem dependencies
	Requires(armSystem);
	SetTimeout(CommandTimeout);
}

// Called just before this Command runs the first time
void ArmInCmd::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void ArmInCmd::Execute()
{
	armSystem->In();
}

// Make this return true when this Command no longer needs to run execute()
bool ArmInCmd::IsFinished()
{
	// default to say the move is finished to stop movement
	bool return_value = true;

	// If the move is not timed out continue the move
	if ( !(IsTimedOut()) )
	{
		return_value = false;
	}

	return (return_value);
}

// Called once after isFinished returns true
void ArmInCmd::End()
{
	armSystem->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void ArmInCmd::Interrupted()
{
	armSystem->Stop();
}

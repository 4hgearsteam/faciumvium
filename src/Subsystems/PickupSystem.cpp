#include "PickupSystem.h"
#include "../RobotMap.h"

PickupSystem::PickupSystem() : Subsystem("Pickup"), m_motor(MOTOR_PICKUP),  m_active(false) {
	Off();
	m_motor.SetInverted(true);
}

void PickupSystem::InitDefaultCommand() {
	// Set the default command for a subsystem here.
	// SetDefaultCommand(new MySpecialCommand());
}

// Put methods for controlling this subsystem
// here. Call these from Commands.

void PickupSystem::Toggle() {
	if (m_active) {
		Off();
	}
	else {
		On();
	}
}


void PickupSystem::On() {
    m_active = true;
    m_motor.Set(pickup_power_on);
}



void PickupSystem::Off() {
    m_active = false;
    m_motor.Set(pickup_power_off);
}

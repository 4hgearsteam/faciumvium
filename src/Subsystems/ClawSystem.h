#ifndef ClawSystem_H
#define ClawSystem_H

#include "Commands/Subsystem.h"
#include "WPILib.h"

class ClawSystem: public Subsystem
{
private:
	DoubleSolenoid m_doubleSolenoid;

public:
	ClawSystem();
	void InitDefaultCommand();
	void Open(void);
	void Close(void);
	void Stop();
	float GetPosition();
};

#endif

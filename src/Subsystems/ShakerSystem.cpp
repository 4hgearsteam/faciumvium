#include "ShakerSystem.h"
#include "../RobotMap.h"
#include "Commands/BallEgressToggleCmd.h"

ShakerSystem::ShakerSystem() : Subsystem("Shaker"), m_shaker(MOTOR_BALL_SHAKE),  m_active(false) {
	Off();
	m_shaker.SetInverted(true);

}

void ShakerSystem::InitDefaultCommand() {
	// Set the default command for a subsystem here.
	// SetDefaultCommand(new MySpecialCommand());
}

void ShakerSystem::Toggle() {
	if (m_active) {
		Off();
	}
	else {
		On();
	}
}
void ShakerSystem::On(){
    m_shaker.Set(1);
}
void ShakerSystem::Off(){
	m_shaker.Set(shaker_power_off);
}
// Put methods for controlling this subsystem
// here. Call these from Commands.

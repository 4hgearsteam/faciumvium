/*
 * PiDigitalInputSubsystems.cpp
 *
 *  Created on: Feb 21, 2017
 *      Author: User
 */

#include "PiDigitalInputSubsystems.h"
#include "../RobotMap.h"
#include "WPILib.h"

PiDigitalInputSubsystems::PiDigitalInputSubsystems() : Subsystem("Pi Input"), Lefty(PI_LEFT_INPUT), Righty(PI_RIGHT_INPUT)
{

}

void PiDigitalInputSubsystems::InitDefaultCommand() {
	// Set the default command for a subsystem here.
	// SetDefaultCommand(new MySpecialCommand());





}
int PiDigitalInputSubsystems::ReadInput()
{
    if(Lefty.Get() && Righty.Get()) {
    	return 3;
    }
    else if (Lefty.Get())
    {
    	return 1;
    }
    else if (Righty.Get()) {
    	return 2;
    }
    else {
    	return 0;
    }
}




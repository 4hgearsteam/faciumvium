#ifndef ArmSystem_H
#define ArmSystem_H

#include "Commands/Subsystem.h"
#include "WPILib.h"

class ArmSystem: public Subsystem
{
private:
	DoubleSolenoid m_doubleSolenoid;

public:
	ArmSystem();
	void InitDefaultCommand();
	void Out(void);
	void In(void);
	void Stop();
	float GetPosition();
};

#endif

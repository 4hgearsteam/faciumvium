#ifndef WinchSystem_H
#define WinchSystem_H

#include <Commands/Subsystem.h>
#include "WPILib.h"

class WinchSystem : public Subsystem {
private:
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities
	VictorSP m_winch_motor;
	bool m_armed;

public:
	WinchSystem();
	void InitDefaultCommand();
	void Move(double power);
	void Stop(void);
	void Disarm(void);
	void Arm(void);
};

#endif  // WinchSystem_H

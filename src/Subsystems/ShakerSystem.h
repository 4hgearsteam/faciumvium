#ifndef ShakerSystem_H
#define ShakerSystem_H

#include <Commands/Subsystem.h>
#include "WPILib.h"


const double shaker_power_on = .30;
const double shaker_power_off = 0.0;

class ShakerSystem : public Subsystem {
private:
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities
	VictorSP m_shaker;
	bool m_active;

public:
	ShakerSystem();
	void InitDefaultCommand();
	void Toggle(void);
	void On(void);
	void Off(void);
};

#endif  // ShakerSystem_H

#include <Subsystems/ArmSystem.h>
#include <Commands/ArmStopCmd.h>
#include <Commands/ArmInCmd.h>
#include <Commands/ArmOutCmd.h>
#include "../RobotMap.h"

ArmSystem::ArmSystem() :
		Subsystem("Arm System"), m_doubleSolenoid(ARM_PCM, ARM_OUT_CHANNEL, ARM_IN_CHANNEL)
{

}

void ArmSystem::InitDefaultCommand()
{
	// Set the default command for a subsystem here.
	SetDefaultCommand(new ArmStopCmd());
}

// Put methods for controlling this subsystem
// here. Call these from Commands.

void ArmSystem::Out(void)
{
	m_doubleSolenoid.Set(DoubleSolenoid::kForward);
}

void ArmSystem::In(void)
{
	m_doubleSolenoid.Set(DoubleSolenoid::kReverse);
}

void ArmSystem::Stop()
{
	m_doubleSolenoid.Set(DoubleSolenoid::kOff);
}

float ArmSystem::GetPosition()
{
	return (0.5);
}

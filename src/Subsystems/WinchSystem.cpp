#include "WinchSystem.h"
#include "../RobotMap.h"

WinchSystem::WinchSystem() : Subsystem("WinchSubsystem"), m_winch_motor(MOTOR_WINCH), m_armed(false)
{
	Stop();
}

void WinchSystem::InitDefaultCommand()
{
	// Set the default command for a subsystem here.
	// SetDefaultCommand(new MySpecialCommand());
}

// Put methods for controlling this subsystem
// here. Call these from Commands.

void WinchSystem::Move(double power)
{
	if (m_armed)
	{
		m_winch_motor.Set(power);
	}
}


void WinchSystem::Stop(void)
{
	m_winch_motor.Set(0.0);
}


void WinchSystem::Disarm(void)
{
	m_armed = false;
}

void WinchSystem::Arm(void)
{
	m_armed = true;
}

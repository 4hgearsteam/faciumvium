/*
 * PiDigitalInputSubsystems.h
 *
 *  Created on: Feb 21, 2017
 *      Author: User
 */

#ifndef SRC_SUBSYSTEMS_PIDIGITALINPUTSUBSYSTEMS_H_
#define SRC_SUBSYSTEMS_PIDIGITALINPUTSUBSYSTEMS_H_

#include <Commands/Subsystem.h>
#include "WPILib.h"

class PiDigitalInputSubsystems : public Subsystem {
private:
	DigitalInput Lefty;
	DigitalInput Righty;
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities

public:
	PiDigitalInputSubsystems();
	void InitDefaultCommand();
	int ReadInput();

};

#endif
/* SRC_SUBSYSTEMS_PIDIGITALINPUTSUBSYSTEMS_H_ */

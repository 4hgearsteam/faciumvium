#include "UltrasonicSensor.h"
#include "../RobotMap.h"
#include "WPILib.h"

UltrasonicSensor::UltrasonicSensor() : Subsystem("UltrasonicSensor"), ai(3)
{


}

void UltrasonicSensor::InitDefaultCommand() {
	// Set the default command for a subsystem here.
	// SetDefaultCommand(new MySpecialCommand());



}
double UltrasonicSensor::GetDistance()
{
	uint16_t aivalue;
	aivalue = ai.GetValue();
	double distance = (double)aivalue * ai.GetAverageVoltage();
	return distance;
}
void UltrasonicSensor::SendDistance()
{
	uint16_t aivalue;
	ai.SetOversampleBits(4);
	aivalue = ai.GetOversampleBits();
	SmartDashboard::PutNumber("AnalogInputOversampledBits" , aivalue);
	ai.SetAverageBits(2);
	aivalue = ai.GetAverageBits();
	SmartDashboard::PutNumber("AnalogInputAverageBits" , aivalue);
	aivalue = ai.GetValue();
	SmartDashboard::PutNumber("AnalogInputValue", aivalue);
	double distance = (double)aivalue * ai.GetAverageVoltage();
	SmartDashboard::PutNumber("Distance", distance);

}
void UltrasonicSensor::Off()
{

}

// Put methods for controlling this subsystem
// here. Call these from Commands.

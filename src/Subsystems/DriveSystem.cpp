#include <Commands/DriveCmd.h>
#include <Subsystems/DriveSystem.h>
#include <Commands/ReadPiInputCmd.h>
#include "../RobotMap.h"
#include <Commands/Subsystem.h>
#include "WPILib.h"

DriveSystem::DriveSystem() : Subsystem("DriveSystem"),
	 	m_motor_front_left(MOTOR_FRONT_LEFT_CHANNEL), m_motor_rear_left(MOTOR_REAR_LEFT_CHANNEL),
		m_motor_front_right(MOTOR_FRONT_RIGHT_CHANNEL), m_motor_rear_right(MOTOR_REAR_RIGHT_CHANNEL),
		m_robot_drive(m_motor_front_left, m_motor_rear_left, m_motor_front_right, m_motor_rear_right)
{
    m_motor_front_right.SetInverted(true);
    m_motor_rear_right.SetInverted(true);
}

void DriveSystem::InitDefaultCommand()
{
	// Set the default command for a subsystem here.
//	SetDefaultCommand(new DriveCmd());
}

// Put methods for controlling this subsystem
// here. Call these from Commands.

void DriveSystem::Drive(float axis_x, float axis_y, float axis_z)
{


	Joystick& joystick = CommandBase::oi->get_joystick();

	float multiplier = 1.0;

	float new_axis_x = axis_x;// * abs(axis_x);
	float new_axis_y = axis_y;// * abs(axis_y);
	float new_axis_z = axis_z;// * abs(axis_z);

	if(joystick.GetRawButton(HALF_SPEED_BUTTON) == true)
	{
		multiplier = MULTIPLIER;
		new_axis_x = axis_x*multiplier;// * abs(axis_x);
		new_axis_y = axis_y*multiplier;// * abs(axis_y);
		new_axis_z = axis_z*multiplier;// * abs(axis_z);
	}

	m_robot_drive.MecanumDrive_Cartesian(new_axis_x, new_axis_y, new_axis_z, 0.0);

	//SmartDashboard::PutNumber("x", new_axis_x);
	//SmartDashboard::PutNumber("y", new_axis_y);
	//SmartDashboard::PutNumber("z", new_axis_z);


	return;
}


void DriveSystem::Stop(void)
{
	m_robot_drive.StopMotor();
	return;
}

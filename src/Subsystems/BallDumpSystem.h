/*
 * BallDumpSystem.h
 *
 *  Created on: Jan 31, 2017
 *      Author: User
 */

#ifndef SRC_SUBSYSTEMS_BALLDUMPSYSTEM_H_
#define SRC_SUBSYSTEMS_BALLDUMPSYSTEM_H_

#include <Commands/Subsystem.h>

class BallDumpSystem : public Subsystem {
private:
	/*
	 * This Subsystem is designed to control the Ball Dumping mechanism. WIP From Robert Blake. Don't touch plz
	 */
	//VictorSP m_dump_motor;	 // The motor specified in RobotMap (currently 6)
	int m_dump_coundown;     // Decrement 1 tick every 20 milliseconds

public:
	BallDumpSystem();
	void InitDefaultCommand();
	void Dump(void);
	void Reset(void);
	void Stop(void);
};

#endif /* SRC_SUBSYSTEMS_BALLDUMPSYSTEM_H_ */
